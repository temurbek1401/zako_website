import {GetEnLanguage, GetLanguage, GetRuLanguage,SetLocalstorage} from "../../Utilitil";

const initialState = {
    uzLang: true,
};

export const langReducer = (state = initialState, action) => {
    switch (action.type) {
        case "Uzlang":

            SetLocalstorage("Zako","UZ")
            return {uzLang: true}
        case "RuLang":

            SetLocalstorage("Zako","RU")
            return {
                uzLang: false,
            }
        default:

        {!GetRuLanguage()?SetLocalstorage("Zako","RU"):SetLocalstorage("Zako","UZ")}
            return {
                uzLang:true,
            };
    }
};