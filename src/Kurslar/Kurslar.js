import React, { Component } from 'react'
import '../Kurslar/kurslar.css'
import { CoursData } from '../Data/data'
import { Card } from 'react-bootstrap'
import { FaStarHalfAlt } from "react-icons/fa";
import { GiWoodenChair } from "react-icons/gi";
import { AiOutlineClockCircle, AiFillStar } from "react-icons/ai";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink,
    Redirect,
    useParams,
    useRouteMatch
} from "react-router-dom";
import AboutAs from "../Components/AboutAs/AboutAs";

class Kurslar extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return (
            <Router>
            <div className='container'>
                <div className='row'>

                    {
                        CoursData.map(item => (
                            <div key={item.id} className='col-12 col-md-6 ' id={"courseDiv"}>
                                <Card  style={{ width: '100%', position: 'relative' }} className='text-left hover-box-shadow mt-5' >
                                    <NavLink to={item.to}>
                                        <Card.Img variant="top" className='images resizeImg' src={item.img}/>
                                    </NavLink>
                                        <span className='price'>{item.price} ming so'm</span>
                                    <Card.Body>
                                        <Card.Title className='title'>{item.title}</Card.Title>
                                        <Card.Text>
                                            <p className='card_text'>{item.info}</p>
                                            <p className='card_text'>{item.info1}</p>
                                            <p className='card_text'>{item.info2}</p>
                                            <p className='card_text'>{item.info3}</p>
                                            <p className='card_text'>{item.info4}</p>
                                            <p className='card_text'>{item.info5}</p>
                                            <p className='card_text'>{item.info6}</p>
                                            <p className='card_text'>{item.info7}</p>
                                            <p className='card_text'>{item.info8}</p>
                                            <p className='card_text'>{item.info9}</p>
                                            <p className='card_text'>{item.info10}</p>
                                            <p className='card_text'>{item.info11}</p>
                                            <p className='card_text'>{item.info12}</p>
                                        </Card.Text>
                                        <Card.Text style={{ borderTop: '1px solid rgb(194, 194, 194)', paddingTop: '10px' }}>
                                            <div className='row'>
                                                <div className='col-4 pr-0'>
                                                    <AiOutlineClockCircle style={{ color: '#11B67A', fontSize: '16px' }} />
                                                    <span className='cardNumber ml-1 seriy'>120</span>
                                                </div>
                                                <div className='col-5 px-0'>
                                                    <AiFillStar className='starIcon' />
                                                    <AiFillStar className='starIcon' />
                                                    <AiFillStar className='starIcon' />
                                                    <AiFillStar className='starIcon' />
                                                    <FaStarHalfAlt className='starIcon ss' />
                                                    <span className='ml-2 seriy'>(4.5)</span>
                                                </div>
                                                <div className='col-3 pl-0'>
                                                    <GiWoodenChair style={{ color: '#11B67A', fontSize: '16px' }} />
                                                    <span className='ml-1 seriy'>60</span>
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        ))
                    }
                </div>
            </div>
                     
                        <Switch>
                            <Route path='/courses/python'>
                                python
                            </Route>

                            <Route path='/courses/java'>
                                java
                            </Route>
                        </Switch>
                    </Router>
        );
    }
}

export default Kurslar;