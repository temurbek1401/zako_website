import React,{Component} from 'react';
import SecondPage from '../Components/SecondPage';
import SliderRouter from '../Components/Slider';
import TopMenu from '../Components/TopMenu';
import {menu} from '../Components/TopMenu'
import '../css/myComponents.css';
import NavMobile from '../Components/NavbarMobile';
import Footer from '../Components/Footer';
import FooterOn from '../Components/FooterOn';
import NavMenu from '../Components/navbar';
import FirstPage from '../Components/FirstPage';
import RouterInfo from '../Components/Router';
import Kurslar from '../Kurslar/Kurslar';

class MyComponents extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <>
                <TopMenu/>
                {
                    window.innerWidth>800?
                   <NavMenu/> :<NavMobile/>
                }
                {/* <Kurslar/> */}
                {/* <FirstPage/>
                <SecondPage/>
                <RouterInfo/>
                <SliderRouter/>
                <FooterOn/> */}
                {/* <Footer/> */}
            </>
         );
    }
}
 
export default MyComponents;