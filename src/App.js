import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import AboutAs from "./Components/AboutAs/AboutAs";
import Teachers from "./Components/teachers/Teachers";
import FirstPage from "./Components/FirstPage";
import Footer from "./Components/Footer";
import TopMenu from "./Components/TopMenu";
import NavMenu from "./Components/navbar";
import {connect} from 'react-redux';
import {UzLanguage} from "./Redux/Actions/UzLanguage";
import {RuLanguage} from "./Redux/Actions/RuLanguage";
import {GetRuLanguage} from "./Utilitil";
import {GetLanguage} from "./Utilitil";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink,
    Redirect,
    useParams,
    useRouteMatch
} from "react-router-dom";
import Kurslar from "./Kurslar/Kurslar";
import SecondPage from "./Components/SecondPage";
import RouterInfo from "./Components/Router";
import SliderRouter from "./Components/Slider";
import {Nav} from "react-bootstrap";

class App extends React.Component{
     componentDidMount() {
         if (!GetLanguage()){
             this.props.UzLanguage();
         }else {
                 this.props.RuLanguage()
             }
         }



   render() {
     return(
         <>
         <div style={{backgroundColor:'#F4F4F4'}}>
           <Router>
               <Switch>
                   <Route exact path='/homepage'>
                       <TopMenu/>
                       <NavMenu/>
                       <FirstPage />
                       <SecondPage/>
                       <RouterInfo/>
                       <SliderRouter/>
                       <Footer/>
                   </Route>
                   <Route path='/about'>
                       <NavMenu/>
                       <AboutAs/>
                       <Footer/>
                   </Route>
                   <Route path='/courses'>
                       <TopMenu/>
                       <NavMenu/>
                       <Kurslar/>
                       <Footer/>
                   </Route>
                   <Route path='/teachers'>
                       <TopMenu/>
                       <NavMenu/>
                       <Teachers/>
                       <Footer/>
                   </Route>
                   <Route path='/blogs'>
                       <TopMenu/>
                       <NavMenu/>
                       <Footer/>
                   </Route>

        <Redirect from={"/"} to={"/homepage"}/>
               </Switch>
            </Router>
            {/*<Kurslar/>*/}
                     </div>
         </>
     )
   }
 }
const mapStateToProps = (state) => {

    return {
        uzLang: state.changeLang.uzLang,
    };
};
export default connect(mapStateToProps,{UzLanguage,RuLanguage})(
    App
)

