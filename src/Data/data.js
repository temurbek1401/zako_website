import PythonImg from '../images/python.jpg'
import JavaImg from '../images/java.jpg'
import FrontImg from '../images/front.jpg'
import CPlus from '../images/c++.jpg'
export const CoursData=[
    {
        to:'/courses/python',
        id:1,
        img:PythonImg,
        title:'Python',
        price:'500 000',
        info:"Kurs vaqtini hammani imkoniyatini hisobga olgan holdan kechki vaqtlarga qo'yishga qaror qildik.",
        info2:"Kurs davomida siz quyidagi texnalogiyalar o'rganish  imkoniga ega bo'lasiz:",
        info3:"Python core",
        info4:"Pythonda OOP",
        info5:"Python telegram bot API",
        info6:"Python MySQL",
        info7:"Django framework",
        info8:"Kurs haqida qo'shimcha ma'lumotlar:",
        info9:"O'quv kurslari haftada 3 marta 2 soatdan.",
        info10:"Kurs narxi oyiga 390ming so'mdan",
        info11:"Kurs davomiyligi 6oy.",
        info12:"1-dars bepul.",
    },
    {
        to:'/courses/java',
        id:2,
        img:JavaImg,
        title:'Java BackEnd',
        price:'500 000',
        info:"Bugungi kunda butun duyoda 3 milliarddan oshiq uskuna Java platformasida ishlar ekan. Bu son kundan kunga oshib bormoqda. O'z o'rnida Java dasturchilariga bo'lgan talab oshishini ham ko'rsatadi.",
        info2:"Chunki, Java dasturlash tili universal va kuchli til bo'lib, u bilan juda ko'plab loyihalar va dasturlar yaratish mumkin.  Shu ustunliklarni hisobga olgan holda, biz ham albatta kurslarimiz ro'yxatiga Java Back-end kursini joylashtirganmiz Olti oylik Back-End kursimizda quyidagi texnalogiyalar o'rgatiladi:",
        info3:"Java Core, Java OOP",
        info4:"Json,  API,  Telegram bot ",
        info5:"Databases, Spring boot",
        info6:"Security, REST API,  React js",
        info7:"Kursimizga asosan html va css biladigan yoki Front-Endni butkul tugallagan talabalar qabul qilinadi.  Sizda ham Java dasturlash tiliga bo'lgan qiziqish bo'lsa, ro'yxatdan o'tishni unutmang",
    },
    {
        to:'/couses/front',
        id:2,
        img:FrontImg,
        title:'Front-End kunduzgi guruhlarga qabul boshlandi!',
        price:'500 000',
        info:"Ko'pchilikni talabini hisobga olgan holda kunduzgi guruhlarga qabulni boshladik.",
        info2:"Darslar 14:30-16:30 oraliqda bo'lib o'tadi.",
        info3:"Kurs davomida siz quyidagi texnalogiyalar o'rganish  imkoniga ega bo'lasiz:",
        info4:"Html5, css3, sass, Bootstrap, JavaScript, Jquery, Git, React js",
        info5:"Qo'shimcha:",
        info6:"Har bir dars kichik guruhlarda amaliy olib boriladi.",
        info7:"Yaxshi bitirgan o'quvchi ish bilan ta'minlanadi.",
        info8:"1-dars bepul.",
    },
    {
        to:'/courses/dasturlash',
        id:2,
        img:CPlus,
        title:"Dasturlashni endi boshlamoqchi bo'lganlar uchun maxsus C++ kurslari",
        price:'500 000',
        info:"Darslarni TATU o'qituvchisi, Respublika chempioni , Xalqaro musobaqalarda TATU shanini munosib himoya qilib kelayotgan Hojiyev Sunatullo o'tadilar"
    },
]