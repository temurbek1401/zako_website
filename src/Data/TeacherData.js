import Umarali from '../images/umarali.jpg'
import Abdusalom from '../images/abdusalom.png'
import Nurbek from '../images/nurbek1.jpg'
import Azimjon from '../images/Azimjon.jpg'

export const Teachers=[
    {
        img:Umarali,
        name:'Umarali',
        lastName:'Mengliyev',
        texnology:'Front End 3-4 yillik tajriba (html, css, bootstrap, sass, js, jQuery, React)',
        tex2:'Back end 1-2 yillik tajriba (php, yii2, mysql)'
    },
    {
        img:Abdusalom,
        name:'Abdusalom',
        lastName:'Sappahonov',
        texnology:'Toshkent Axborot Texnalogiyalari Univeristeti 3-kurs talabasi.',
        tex2:"SMM mutahasisi sifatida Zako IT Akademiyasida o'z ish faoliyatini boshlagan."
    },
    {
        img:Umarali,
        name:'Umarali',
        lastName:'Mengliyev',
        texnology:'Front End 3-4 yillik tajriba (html, css, bootstrap, sass, js, jQuery, React)',
        tex2:'Back end 1-2 yillik tajriba (php, yii2, mysql)'
    },
    {
        img:Abdusalom,
        name:'Abdusalom',
        lastName:'Sappahonov',
        texnology:'Toshkent Axborot Texnalogiyalari Univeristeti 3-kurs talabasi.',
        tex2:"SMM mutahasisi sifatida Zako IT Akademiyasida o'z ish faoliyatini boshlagan."
    },
    {
        img:Nurbek,
        name:'Nurbek',
        lastName:'Xolmurodov',
        texnology:'Front End 3-4 yillik tajriba (html, css, bootstrap, sass, js, jQuery, React)',
        tex2:'Back end 1-2 yillik tajriba (php, yii2, mysql)'
    },
    {
        img:Azimjon,
        name:'Azimjon',
        lastName:'Norqobilov',
        texnology:'2015 yildan 2019 yilga qadar Termiz Davlat Universitetida tahsil olgan. Hozirda Milliy Universitet 2-kurs',
        tex2:"magistranti. Quydagi dasturlash tillarini mukammal egallagan:"
    },
]