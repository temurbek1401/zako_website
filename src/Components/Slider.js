import React, { Component } from 'react';
import Slider from "react-slick";
import { Card } from 'react-bootstrap';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Teachers} from '../Data/TeacherData'
import Nurbek from '../images/nurbek1.jpg'
import '../css/slider.css'
import ReactResizeDetector from 'react-resize-detector';
import { FaFacebookF, FaTwitter, FaTelegramPlane, FaInstagram } from "react-icons/fa";


class SliderRouter extends Component {
   state={
       number:4
   }
   Resize = () =>{
         if(window.innerWidth<992 && window.innerWidth>552){
             this.setState({
                 number:2
             })
         }else if(window.innerWidth<552){
             this.setState({
                 number:1
             }) 
         }else{
            this.setState({
                number:4
            }) 
         }
     }
   render() {
        this.componentDidMount=()=>{
            this.Resize()
        }
        const {number}=this.state;
        var settings = {
            dots: false,
            infinite: true,
            horisantal: true,
            focusOnSelect: true,
            swipeToSlide: true,
            verticalSwiping: false,
            slidesToShow: number,
        };
        return (
            
            <div className='container  bg-light' style={{marginTop:'-10px'}}>
            <ReactResizeDetector handleWidth handleHeight onResize={this.Resize}/>
            <p className='text-center mt-2 pt-1 kurslar' >O'qituvchilar</p>
                <div id='tovar_catalog' >
                    <Slider {...settings}>
                        {
                            Teachers.map(item=>(
                        <div style={{textAlign:'center'}}>
                            <Card className='Slider-Item sliderReponsive' style={{width:'95%'}}>
                                <Card.Img variant="top" src={item.img} />
                                    <div className='Text_slider pb-2'>
                                        <Card.Title className='sl-post-cat'>O'qituvchi</Card.Title>
                                        <span className='textBottom'>{item.name} {item.lastName}</span>
                                        <div className='iconss'>
                                            <span><a href='#' className='ml-2 facebookColor'><FaFacebookF /></a></span>
                                            <span><a href='#' className='telegramColor'><FaTelegramPlane/></a></span>
                                            <span><a href='#' className='instaColor'><FaInstagram /></a></span>
                                        </div>
                                    </div>
                                <div className='clearfix'></div>
                            </Card>
                        </div>
                            ))
                        }
                    </Slider>
                </div>
            </div>
        );
    }
}

export default SliderRouter;