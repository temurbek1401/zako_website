import '../css/navbar.css'
import React, { Component } from 'react';
import Logo from '../images/logo.png';
import { IoIosSearch } from "react-icons/io";
import FirstPage from './FirstPage';
import {MenuOutlined}  from '@ant-design/icons';
import {Row,Col} from 'antd';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
  Redirect,
  useParams,
  useRouteMatch
} from "react-router-dom";
import FooterOn from './FooterOn';
import Footer from './Footer';
import Kurslar from '../Kurslar/Kurslar';
import SecondPage from './SecondPage';
import RouterInfo from './Router';
import SliderRouter from './Slider';
import Teachers from './Teachers';
import AboutAs from './AboutAs';

class NavMenu extends Component {
  state = {
    className: "ss",
    isSearch: false,
    plus:''
  };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  handleSearch = () => {
    this.setState(prevState => ({
      isSearch: !prevState.isSearch
    }));
  }

  open=()=> {
    {
      document.querySelector(".sidebarmenu").classList.toggle("cancel");
      // document.querySelector(".top_menu").classList.add("cancel");
      // document.querySelector(".searchInput").classList.add("cancel");
      this.setState({
        plus: '',
      })
    }
  }
    handleScroll = () => {

    if (window.pageYOffset > 0) {
      if (!this.state.className) {
        this.setState({ className: "backColor" });
      }
    } else {
      if (this.state.className) {
        this.setState({ className: "White" })
      }
    }

  }

  render() {
    const { isSearch } = this.state;
    return (
      <main className='top aaaa'>
          <Router>
          <header ref={(r) => this.ref = r} className={this.state.className}>
            <div className='container navbar_info' >

              <div className='row'>
                <div className=' left_1 px-0'>
                  <img src={Logo} alt='error img' className='logo_img' />
                </div>
                <div className='left_2 px-0'>
                  <ul className="top_menu">
                    <li className='mx-3 navbar_link'>
                      <NavLink activeClassName='navActiveClass' to={"/homepage"}>BOSH SAHIFA</NavLink>
                    </li>
                    <li className='mx-3 navbar_link'>
                      <NavLink activeClassName='navActiveClass' to={"/about"}>BIZ HAQIMIZDA</NavLink>
                    </li>
                    <li className='mx-3 navbar_link'>
                      <NavLink activeClassName='navActiveClass' to={"/courses"}>KURSLAR</NavLink>
                    </li>
                    <li className='mx-3 navbar_link'>
                      <NavLink activeClassName='navActiveClass' to={"/teachers"}>O'QITUVCHILAR</NavLink>
                    </li>
                    <li className='mx-3 navbar_link'>
                      <NavLink activeClassName='navActiveClass' to={"/blogs"}>BLOG</NavLink>
                    </li>
                  </ul>
                </div>
                <p  onClick={this.open} className="none" style={{position:'absolute',right:'10px',top:'2px',fontSize:'30px',cursor:'pointer'}}>
                  <MenuOutlined/>
                </p>

                <div className='left_3 px-0'>
                  <IoIosSearch className='searchIcon ml-0' onClick={this.handleSearch} />

                  {
                    isSearch ?
                      <div>
                        {/* <Button variant="outline-primary" className='searchButton p-1 mt-3 ml-3'>Search</Button> */}
                        <input placeholder='Search type...' type='text' className='searchInput mt-3 p-1' />
                      </div>
                      : ''
                  }
                </div>

              </div>
              <div className="sidebarmenu cancel">
                <Row>
                  <Col sm={24} className="k-btn ">
                    <p style={{fontSize:'22px',color:'white',marginLeft:'20px',marginTop:'7px'}}><strong>ZAKO IT Academy</strong></p>
                  </Col>

                    <div className="umumiy ">
                      Bosh Sahifa
                    </div>
                  <div className="umumiy ">
                    Biz haqimizda
                  </div>
                  <div className="umumiy ">
                    Kurslar
                  </div>
                  <div className="umumiy ">
                    O'qituvchilar
                  </div>
                  <div className="umumiy ">
                    Blog
                  </div>
                  <Col sm={24} className="umumiy">
                    Struktura
                  </Col>
                </Row>

              </div>
            </div>
          </header>
          <div>
            {/* <FirstPage/> */}
          </div>

      </Router>

        </main>
    );
  }
}
export default NavMenu;