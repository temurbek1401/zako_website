import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../AboutAs/About.css';
import {Card,Form,Button} from "react-bootstrap";
import {FaTelegram,FaInstagram,FaTwitter,FaFacebook} from 'react-icons/fa';
import AOS from  'aos';
class AboutAs extends React.Component{

Open=()=>{
    document.querySelector(".switchdiv").classList.toggle("close");
}
    Open1=()=>{
        document.querySelector(".switchdiv1").classList.toggle("close");
    }
    Open2=()=>{
        document.querySelector(".switchdiv2").classList.toggle("close");
    }

    render() {
        AOS.init();
        return(
            <section className="body" >

               <div data-aos={"fade-left"} className="w-100" style={{backgroundColor:'#8C95A4'}}>
                   <div className="container row" style={{margin:'0 auto'}}>
                       {/*<div className="col-4" style={{width:'100%'}}><img width="100%" height="40%" style={{margin:'13% auto'}} src="http://195.158.24.249:2025/api/client/file/prewiev/2MxaVn" alt="error"/></div>*/}
                       <div className="col-lg-4 col-12" style={{width:'100%',margin:'0 auto'}}> <div  style={{backgroundColor:'#182B49',borderRadius:'10px',margin:'8% auto',height:'140px'}}>
                          <h3 style={{color:'white',marginLeft:'20%',paddingTop:'40px'}}>BIZ HAQIMIZDA</h3>
                           <p style={{color:'#12AA63',marginLeft:'20%',float:'left',cursor:'pointer'}}>Bosh sahifa </p>
                           <p style={{color:'white',marginLeft:'20%'}}>/ Biz haqimizda</p>
                       </div></div>
                       </div>
               </div>


                <div className="container bg-light h-50 p-2 mt-2" >

                    <Card style={{ width: '100%' ,border:'0px',marginTop:'20px'}}>
                        <Card.Img variant="top" src="http://195.158.24.249:2025/api/client/file/prewiev/gVzx1n" />
                        <Card.Body>
                            <Card.Text>
                                <Card.Title> <strong className="jamoa" style={{marginLeft:'40%',color:'#EB196D'}}>ZAKO  <br/> <p style={{marginLeft:'20%',color:'black'}}> Axborot Texnologiyalari Akademiyasi</p></strong></Card.Title>
                               <p style={{fontFamily:'sans-serif'}}>Akademiya 2019-yil boshlarida tashkil topgan. Avvaliga TATU(Toshkent Axborot TexnalogiyalariUniversiteti) ichida faoliyat olib borgan. U vaqtdafaqatgina kichik yoshdagilar uchun komyutersavodxonligi, kichik o'yinlar yaratish vaRobotexnika o'rgatilgan.Keyinchalik yil oxiriga kelib dasturlash kurslariham o'tila boshlagan va alohida o'quv markazsifatida faoliyat olib borgan.
                                  <br/></p><h6> Asoschisi Farrux Boltayev</h6>
                            </Card.Text>
                        </Card.Body>
                        <Card.Body>
                            <Card.Link href="#">
                                <ul className="linkUl" >
                                    <li ><div className="iconLink"><FaTelegram/></div></li>
                                    <li>Telegram</li>
                                    <li><div className="iconLink"><FaInstagram/></div></li>
                                    <li>Instagram</li>
                                    <li><div className="iconLink"><FaFacebook/></div></li>
                                    <li> Facebook</li>
                                </ul> </Card.Link>
                            </Card.Body>
                    </Card>

                </div>
                <div className="container mt-5">
                    <h5>Nima uchun Zakoni tanlaymiz?</h5>
                    <div className="bg-light">
                        <Form>
                            <div className="row">
                              <div className="col-lg-4 col-12" style={{padding:'20px'}}>
                                  <Form.Check
                                      onChange={this.Open}
                                      className="formcheck"
                                      type="switch"
                                      id="custom-switch"
                                      label={<h6>Yoshlari qamrab olamiz</h6>}
                                  />
                                  <div className="switchdiv close" >
                                      <p>Hozirgi kunga kelib yurtimizda IT sohasiga bo’lgan talab juda ham ortib ketdi. Sababi, IT sohasida yoshlarning diplomsiz ham o’z mustahkam o’rniga ega bo’lishi, asosiy omillardan biri. Shu sababli, biz IT sohasiga qiziqqan yoshlarni qidirib topamiz.</p>

                                   <div className="switchfooter"><h3>START</h3></div>
                                  </div>
                              </div>
                              <div className="col-lg-4 col-12"  style={{padding:'20px'}}>
                                  <Form.Check
                                      onChange={this.Open1}
                                      type="switch"
                                      label={<h6>Dasturlashni mukammal o'rgatamiz</h6>}
                                      id="disabled-custom-switch"
                                  />
                                  <div className="switchdiv1 close">
                                      <p>Eng yaxshi investitsiya - bu ta’limga kiritilgan investitsiyadir. Hozirgi texnologiya asrida IT sohasini o’rganish “must have” hisoblanadi. Oliygohda 4 yil davomida o’rganishi kerak bo’lgan narsani 1 yil ichida ham mukammal o’rgansa bo’ladi va biz ishni amalga oshirish uchun harakat qilamiz.</p>

                                      <div className="switchfooter1"><h3>DO</h3></div> </div>
                              </div>
                              <div className="col-lg-4 col-12"  style={{padding:'20px'}}>
                                  <Form.Check
                                      onChange={this.Open2}
                                      type="switch"
                                      label={<h6>Ishga joylashishiga ko'maklashamiz</h6>}
                                      id="disabled1-custom-switch"
                                  />
                                  <div className="switchdiv2 close">
                                      <p>6-10 oy davomida chuqurlashtirilgan ta’limni olgandan so’ng, o’quvchida keng imkoniyatlar eshigi ochiladi. Asosiysi, diplomsiz, hech qanday tanish-bilishsiz katta kompaniyalarda faoliyat olib borishi mumkin bo’ladi va biz bu yo’lda o’quvchilarimizga ko’mak bo’lamiz.</p>
                                      <div className="switchfooter2"><h3>SUCCESS</h3></div>
                                  </div>
                              </div>
                            </div>
                        </Form>

                    </div>
                    <div className="container bg-light mt-4" style={{padding:'20px'}}>
                        <div className="row">
                            <div className="col-md-6 col-12" ><img src="https://najottalim.uz/images/live-chat.png" alt="error" style={{float:'left'}}/><strong style={{fontSize:'30px'}}>ALOQA OPERATORI</strong></div>
                            <div className="col-md-3 col-12"><strong style={{fontSize:'20px'}}>+998 (99) 316-66-26</strong> <p>8:00 dan 22:00 gacha</p></div>
                            <div className="col-md-3 col-12"><Button variant="dark">Qo'ng'iroq qilish</Button></div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
export default AboutAs;
