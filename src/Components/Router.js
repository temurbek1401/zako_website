import React, { Component } from 'react'
import '../css/router.css'
import { Card } from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink,
    Redirect,
    useParams,
    useRouteMatch
} from "react-router-dom";
import { CoursData } from '../Data/data'
import PythonImg from '../images/python.jpg'
import { AiOutlineClockCircle, AiFillStar } from "react-icons/ai";
import { FaStarHalfAlt } from "react-icons/fa";
import { GiWoodenChair } from "react-icons/gi";
import JavaImg from '../images/java.jpg'
import FrontImg from '../images/front.jpg'
import CPlus from '../images/c++.jpg'
import Python from './pythonComponent';
import TopMenu from './TopMenu';
import NavMenu from './navbar';
import NavMobile from './NavbarMobile';
import FirstPage from './FirstPage';
import SecondPage from './SecondPage';
import SliderRouter from './Slider';
import FooterOn from './FooterOn';
import Footer from './Footer';

class RouterInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className='routerBest mt-5'>
                <div className='container'>
                    <p className='text-center mt-5 pt-4 kurslar'>Kurslar</p>
                    <div className='navRouter py-3 text-center'>
                        <Router>
                            <ul>
                                <li>
                                    <NavLink exact to='/' activeClassName='activeBtn'>Barchasi</NavLink>
                                </li>
                                <li>
                                    <NavLink to='backEnd' activeClassName='activeBtn'>BackEnd</NavLink>
                                </li>
                                <li>
                                    <NavLink to='frontEnd' activeClassName='activeBtn'>FrontEnd</NavLink>
                                </li>
                                <li>
                                    <NavLink to='dasturlash' activeClassName='activeBtn'>Dasturlash</NavLink>
                                </li>
                            </ul>
                            <div style={{ clear: 'both' }}></div>
                            <Switch>
                                <Route exact path="/">
                                    <Redirect exact from="/" to="/" />
                                    <div className='row'>
                                            {
                                                CoursData.map(item => (
                                                    <div key={item.id} className='col-12 col-sm-6 col-lg-4  text-center'>
                                                        <NavLink to={item.to}>
                                                            <Card style={{ width: '100%', position: 'relative' }} className='text-left mt-5'>
                                                                <Card.Img variant="top" className='images' src={item.img} />
                                                                <span className='price'>{item.price} ming so'm</span>
                                                                <Card.Body>
                                                                    <Card.Title className='title'>{item.title}</Card.Title>
                                                                    <Card.Text>
                                                                        <div className='row'>
                                                                            <div className='col-4 pr-0'>
                                                                                <AiOutlineClockCircle style={{ color: '#11B67A', fontSize: '16px' }} />
                                                                                <span className='cardNumber ml-1 seriy'>120</span>
                                                                            </div>
                                                                            <div className='col-5 px-0'>
                                                                                <AiFillStar className='starIcon' />
                                                                                <AiFillStar className='starIcon' />
                                                                                <AiFillStar className='starIcon' />
                                                                                <AiFillStar className='starIcon' />
                                                                                <FaStarHalfAlt className='starIcon ss' />
                                                                                <span className='ml-2 seriy'>(4.5)</span>
                                                                            </div>
                                                                            <div className='col-3 pl-0'>
                                                                                <GiWoodenChair style={{ color: '#11B67A', fontSize: '16px' }} />
                                                                                <span className='ml-1 seriy'>60</span>
                                                                            </div>
                                                                        </div>
                                                                    </Card.Text>
                                                                </Card.Body>
                                                            </Card>
                                                        </NavLink>
                                                    </div>
                                                ))
                                            }
                                    </div>
                                </Route>
                                <Route path='/backEnd'>
                                    <div className='col-12 col-sm-6 col-lg-4 text-center'>
                                        <Card style={{ width: '100%', position: 'relative' }} className='text-left mt-5'>
                                            <Card.Img variant="top" className='images' src={JavaImg} />
                                            <span className='price'>500 ming so'm</span>
                                            <Card.Body>
                                                <Card.Title className='title'>Java BackEnd</Card.Title>
                                                <Card.Text>
                                                    <div className='row'>
                                                        <div className='col-4 pr-0'>
                                                            <AiOutlineClockCircle style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='cardNumber ml-1 seriy'>120</span>
                                                        </div>
                                                        <div className='col-5 px-0'>
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <FaStarHalfAlt className='starIcon ss' />
                                                            <span className='ml-2 seriy'>(4.5)</span>
                                                        </div>
                                                        <div className='col-3 pl-0'>
                                                            <GiWoodenChair style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='ml-1 seriy'>60</span>
                                                        </div>
                                                    </div>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Route>
                                <Route path='/frontEnd'>
                                    <div className='col-12 col-sm-6 col-lg-4 text-center'>
                                        <Card style={{ width: '100%', position: 'relative' }} className='text-left mt-5'>
                                            <Card.Img variant="top" className='images' src={FrontImg} />
                                            <span className='price'>500 ming so'm</span>
                                            <Card.Body>
                                                <Card.Title className='title'>Front-End kunduzgi guruhlarga qabul boshlandi!</Card.Title>
                                                <Card.Text>
                                                    <div className='row'>
                                                        <div className='col-4 pr-0'>
                                                            <AiOutlineClockCircle style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='cardNumber ml-1 seriy'>120</span>
                                                        </div>
                                                        <div className='col-5 px-0'>
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <FaStarHalfAlt className='starIcon ss' />
                                                            <span className='ml-2 seriy'>(4.5)</span>
                                                        </div>
                                                        <div className='col-3 pl-0'>
                                                            <GiWoodenChair style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='ml-1 seriy'>60</span>
                                                        </div>
                                                    </div>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Route>
                                <Route path='/dasturlash'>
                                    <div className='col-12 col-sm-6 col-lg-4 text-center'>
                                        <Card style={{ width: '100%', position: 'relative' }} className='text-left mt-5'>
                                            <Card.Img variant="top" className='images' src={CPlus} />
                                            <span className='price'>500 ming so'm</span>
                                            <Card.Body>
                                                <Card.Title className='title'>Dasturlashni endi boshlamoqchi bo'lganlar uchun maxsus C++ kurslari</Card.Title>
                                                <Card.Text>
                                                    <div className='row'>
                                                        <div className='col-4 pr-0'>
                                                            <AiOutlineClockCircle style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='cardNumber ml-1 seriy'>120</span>
                                                        </div>
                                                        <div className='col-5 px-0'>
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <AiFillStar className='starIcon' />
                                                            <FaStarHalfAlt className='starIcon ss' />
                                                            <span className='ml-2 seriy'>(4.5)</span>
                                                        </div>
                                                        <div className='col-3 pl-0'>
                                                            <GiWoodenChair style={{ color: '#11B67A', fontSize: '16px' }} />
                                                            <span className='ml-1 seriy'>60</span>
                                                        </div>
                                                    </div>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Route>
                            </Switch>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

export default RouterInfo;