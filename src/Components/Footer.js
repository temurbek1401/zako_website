import React,{Component} from 'react'
import '../css/footer.css'
import { GoLocation } from "react-icons/go";
import { AiFillHeart } from "react-icons/ai";
import { FaFacebookF, FaTwitter, FaTelegramPlane, FaInstagram,FaYoutube } from "react-icons/fa";

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
        <div className='footerColor'>
            <div className='container'>
                <div className='row py-4'>
                    <div className='col-12 col-md-6'>
                        <span className='seriy mr-2 fs-13 my-0'> &copy; 2021 | Created By </span>
                        <span><AiFillHeart className='zakoColor my-0 mr-2' /></span>
                        <a href='#' className='fs-13 my-2 location zakoColor'>Zako IT Academy</a><br/>
                        <span><GoLocation className='zakoColor my-0' /></span>
                        <span className='seriy mx-2 fs-13 my-0'>Amir Temur kochasi, TATU yonida,</span>
                        <a href='#' className='fs-13 my-2 location zakoColor'>Mo'ljal:Bodomzor metro bekati</a>
                    </div>
                    <div className='col-12 col-md-3'>
                        <p className='telText ml-3  my-0 mt-1'>Telefon raqam:</p>
                        <p className='telNumber ml-3 m-0'>+998 71 200 06 20</p>
                    </div>
                    <div className='col-12 col-md-3'>
                        <div className='iconsss pt-2'>
                            <span><a href='#' className='ml-2 facebookColor'><FaFacebookF /></a></span>
                            <span><a href='#' className='telegramColor'><FaTelegramPlane/></a></span>
                            <span><a href='#' className='instaColor'><FaInstagram /></a></span>
                            <span><a href='#' className='youtube'><FaYoutube /></a></span>
                            <span><a href='#' className='twitter'><FaTwitter /></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        );
    }
}
 
export default Footer;