import React,{Component} from 'react';
import '../css/secondPage.css'
import { AiOutlinePlayCircle } from "react-icons/ai";
import '../css/modalVideo.css';
import ModalVideo from 'react-modal-video'

class SecondPage extends Component {
    constructor () {
        super()
        this.state = {
            isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal=()=> {
        this.setState({
            isOpen: true
        })
    }
    render() { 
        return ( 
            <div>
                <ModalVideo
                 channel='youtube'
                 isOpen={this.state.isOpen} 
                 videoId='D4r63aceJXE' 
                 onClose={() => this.setState({isOpen: false})} 
                 />
                <div className='container'>
                    <div className='row py-5 mt-5 position_relative'>
                        <div className='col-12 col-md-6'>
                            <div className='play_video'>
                                <div className='index'>
                                </div>
                            </div>
                        </div>
                        <AiOutlinePlayCircle onClick={this.openModal} className='play_icon'/>
                        <div className='col-12 col-md-6'>
                            <p className='bigSecond'>ZAKO axborot texnologiyalari akademiyasi</p>
                            <p className='little_p mb-0'>Akademiya 2019-yil boshlarida tashkil topgan. Avvaliga TATU(Toshkent Axborot TexnalogiyalariUniversiteti) ichida faoliyat olib borgan. U vaqtdafaqatgina kichik yoshdagilar uchun komyutersavodxonligi, kichik o'yinlar yaratish vaRobotexnika o'rgatilgan.Keyinchalik yil oxiriga kelib dasturlash kurslariham o'tila boshlagan va alohida o'quv markazsifatida faoliyat olib borgan.</p>
                            <p className='little_p as'>Asoschisi Farrux Boltayev</p>
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default SecondPage;