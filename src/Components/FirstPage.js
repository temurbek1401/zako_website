import React,{Component} from 'react';
import '../Components/css/firstPage.css';
import BigPng from '../images/big.png'
import {connect} from "react-redux";
import {UzLanguage} from "../Redux/Actions/UzLanguage";
import {RuLanguage} from "../Redux/Actions/RuLanguage";

class FirstPage extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {  }
    // }
    render() {
        const {uzLang}=this.props;
        return ( 
            <div className='theBiggest'>
                <div className='container'>
                    <div className='row mmm'>
                        <div className=' col-6 col-md-mb-5 mt-50' >
                            <div className='pt-5' style={{width:'100%'}}>
                                <p className='big_p'>Zako Axborot Texnologiyalari Akademiyasi</p>
                                <p className='small_p'>ZAKO axborot texnologiyalari akademiyasi</p>
                                <button className='myBtn btn text-white'>{uzLang?"Bizning Kurslar":"Our Courses"}</button>
                                <button className='btn myBtn_2 text-white'>Bog'lanish</button>
                            </div>
                        </div>
                        <div className=' col-6' >
                            <img src={BigPng} className='big_png' style={{width:'100%'}} />
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
const mapStateToProps = (state) => {

    return {
        uzLang: state.changeLang.uzLang,
    };
};
export default connect(mapStateToProps,{UzLanguage,RuLanguage})(
    FirstPage
)