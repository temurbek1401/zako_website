import React, { Component } from 'react';
import { GoLocation } from "react-icons/go";
import 'bootstrap/dist/css/bootstrap.min.css';
import { FaFacebookF, FaTwitter, FaTelegramPlane, FaInstagram } from "react-icons/fa";
// import { Menu, Dropdown } from 'antd';
// import { DownOutlined } from '@ant-design/icons';
import '../css/topMenu.css';
import ImgUz from '../images/uz.png';
import ImgRu from '../images/ru.png'
import DropdownLang from './dropdownLang';
import {Form} from "react-bootstrap";
import {connect} from 'react-redux';
import {UzLanguage} from "../Redux/Actions/UzLanguage";
import {RuLanguage} from "../Redux/Actions/RuLanguage";
import {GetLanguage} from "../Utilitil";

class TopMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }
    OpenSwitch=()=>{
        this.props.UzLanguage();
    }
    OpenSwitch1=()=>{
        this.props.RuLanguage();
    }

    render() {
        const {langCheck}=this.state;
    const {uzLang}=this.props;

        return (
            <div className='aa'>
                <div className='container'>
                    <div className='leftTop px-3'>
                        <div className='aa'>
                            <span><GoLocation className='zakoColor my-3' /></span>
                            <span className='seriy mx-2 fs-13 my-3'>Amir Temur {uzLang?"ko'chasi":"street"}, TATU yonida,</span>
                            <a href='#' className='fs-13 my-2 location zakoColor'>Mo'ljal:Bodomzor metro bekati</a>
                        </div>
                    </div>

                    <div className='rightTop_2 mm py-0 px-2'>
                        {/*<DropdownLang/>*/}
                        <Form.Check
                            checked={uzLang?true:false}
                            onChange={this.OpenSwitch}
                            className="formcheck"
                            type="switch"
                            id="custom-uz"
                            label={<h6 style={{color:'white'}}>UZ</h6>}
                        />
                        <Form.Check
                            checked={uzLang?false:true}
                            onChange={this.OpenSwitch1}
                            className="formcheck"
                            type="switch"
                            id="custom-ru"
                            label={<h6 style={{color:'white'}}>RU</h6>}
                        />
                    </div>
                    <div className='rightTop px-2'>
                        <span><a href='#'><FaFacebookF className='my-3 zakoColor mx-1 my-2 fs-13' /></a></span>
                        <span><a href='#'><FaTwitter className='my-3 zakoColor mx-2 my-2 fs-13' /></a></span>
                        <span><a href='#'><FaTelegramPlane className='my-3 zakoColor mx-1 fs-13 my-2' /></a></span>
                        <span><a href='#'><FaInstagram className='my-3 zakoColor mx-2 my-2 fs-13' /></a></span>
                    </div>
                    <div style={{ clear: 'both' }}></div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {

    return {
        uzLang: state.changeLang.uzLang,
    };
};
export default connect(mapStateToProps,{UzLanguage,RuLanguage})(
    TopMenu
)
