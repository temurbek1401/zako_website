import React from 'react';
import '../teachers/Teacher.css';
import {Card,Form,Button} from "react-bootstrap";
import {Pagination} from "antd";
import 'antd/dist/antd.css';
import {FaFacebook, FaInstagram, FaTelegram} from "react-icons/fa";
class Teachers extends  React.Component{
    state={
        activePage:1,
    }
    handlePageChange=(pageNumber)=>{
        console.log(pageNumber)
    }
    OpenInfo=()=>{
        document.querySelector(".switchinfo").classList.toggle("close");
    }
    OpenInfo1=()=>{
        document.querySelector(".switchinfo1").classList.toggle("close");
    }
    OpenInfo2=()=>{
        document.querySelector(".switchinfo2").classList.toggle("close");
    }
    OpenInfo3=()=>{
        document.querySelector(".switchinfo3").classList.toggle("close");
    }
    render() {

        return(
            <section>
             <div>
                 <div className="w-100" style={{backgroundColor:'#8C95A4'}}>
                     <div className="container row" style={{margin:'0 auto'}}>
                         {/*<div className="col-4" style={{width:'100%'}}><img width="100%" height="40%" style={{margin:'13% auto'}} src="http://195.158.24.249:2025/api/client/file/prewiev/2MxaVn" alt="error"/></div>*/}
                         <div className="col-lg-4 col-12" style={{width:'100%',margin:'0 auto'}}> <div  style={{backgroundColor:'#182B49',borderRadius:'10px',margin:'8% auto',height:'140px'}}>
                             <h3 style={{color:'white',marginLeft:'20%',paddingTop:'40px'}}>O'QITUVCHILAR</h3>
                             <p style={{color:'#12AA63',marginLeft:'20%',float:'left',cursor:'pointer'}}>Bosh sahifa </p>
                             <p style={{color:'white',marginLeft:'20%'}}>/ O'qituvchilar</p>
                         </div></div>
                     </div>
                 </div>
                 <div className="row container" style={{margin:'0 auto'}}>
                     <div className="col-lg-6 col-12">
                         <Card style={{ width: '100%' ,border:'0px',marginTop:'20px'}}>
                             <Card.Img variant="top" src="http://195.158.24.249:2025/api/client/file/prewiev/rVgA1m" />
                             <Card.Body>
                                 <Card.Text>
                                     <Card.Title> <strong  style={{color:'#AC0B94'}}>ABDUSALOM SAPPAHONOV  <br/> <p style={{color:'black'}}> TEACHER</p></strong></Card.Title>
                                     <Form.Check
                                         onChange={this.OpenInfo}
                                         className="formcheck"
                                         type="switch"
                                         id="custom-switchh"
                                         label="Qisqacha ma'lumot"
                                     />
                                     <Card style={{ width: '100%' }} className="close switchinfo">
                                         <Card.Body>
                                             <Card.Title>Abdusalom Sappahonov</Card.Title>
                                             <Card.Text>
                                                 Some quick example text to build on the card title and make up the bulk of
                                                 the card's content.
                                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid quas saepe sapiente voluptate. Eos exercitationem harum laboriosam modi officia optio reiciendis soluta suscipit vitae voluptate. Blanditiis nostrum obcaecati quos?
                                             </Card.Text>
                                         </Card.Body>
                                         <Card.Footer style={{padding:'0'}}>
                                             <Button className="infoBtn"><h3>Bog'lanish</h3></Button>
                                         </Card.Footer>
                                     </Card>
                                 </Card.Text>
                             </Card.Body>
                             <Card.Body>
                                 <Card.Link href="#">
                                     <ul className="linkUl1" >
                                         <li ><div className="iconLink1"><FaTelegram/></div></li>
                                         <li>Telegram</li>
                                         <li><div className="iconLink1"><FaInstagram/></div></li>
                                         <li>Instagram</li>
                                         <li><div className="iconLink1"><FaFacebook/></div></li>
                                         <li> Facebook</li>
                                     </ul>
                                 </Card.Link>
                                 </Card.Body>
                         </Card>
                     </div>
                     <div className="col-lg-6 col-12">
                         <Card style={{ width: '100%' ,border:'0px',marginTop:'20px'}}>
                             <Card.Img variant="top" src="http://195.158.24.249:2025/api/client/file/prewiev/rVgA1m" />
                             <Card.Body>
                                 <Card.Text>
                                     <Card.Title> <strong  style={{color:'#AC0B94'}}>ABDUSALOM SAPPAHONOV  <br/> <p style={{color:'black'}}> TEACHER</p></strong></Card.Title>
                                     <Form.Check
                                         onChange={this.OpenInfo1}
                                         className="formcheck"
                                         type="switch"
                                         id="custom-switch1"
                                         label="Qisqacha ma'lumot"
                                     />
                                     <Card style={{ width: '100%' }} className="close switchinfo1">
                                         <Card.Body>
                                             <Card.Title>Abdusalom Sappahonov</Card.Title>
                                             <Card.Text>
                                                 Some quick example text to build on the card title and make up the bulk of
                                                 the card's content.
                                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid quas saepe sapiente voluptate. Eos exercitationem harum laboriosam modi officia optio reiciendis soluta suscipit vitae voluptate. Blanditiis nostrum obcaecati quos?
                                             </Card.Text>
                                         </Card.Body>
                                         <Card.Footer style={{padding:'0'}}>
                                             <Button className="infoBtn"><h3>Bog'lanish</h3></Button>
                                         </Card.Footer>
                                     </Card>
                                 </Card.Text>
                             </Card.Body>
                             <Card.Body>
                                 <Card.Link href="#">
                                     <ul className="linkUl1" >
                                         <li ><div className="iconLink1"><FaTelegram/></div></li>
                                         <li>Telegram</li>
                                         <li><div className="iconLink1"><FaInstagram/></div></li>
                                         <li>Instagram</li>
                                         <li><div className="iconLink1"><FaFacebook/></div></li>
                                         <li> Facebook</li>
                                     </ul>
                                 </Card.Link></Card.Body>
                         </Card>
                     </div>
                     <div className="col-lg-6 col-12">
                         <Card style={{ width: '100%' ,border:'0px',marginTop:'20px'}}>
                             <Card.Img variant="top" src="http://195.158.24.249:2025/api/client/file/prewiev/rVgA1m" />
                             <Card.Body>
                                 <Card.Text>
                                     <Card.Title> <strong  style={{color:'#AC0B94'}}>ABDUSALOM SAPPAHONOV  <br/> <p style={{color:'black'}}> TEACHER</p></strong></Card.Title>
                                     <Form.Check
                                         onChange={this.OpenInfo2}
                                         className="formcheck"
                                         type="switch"
                                         id="custom-switch2"
                                         label="Qisqacha ma'lumot"
                                     />
                                     <Card style={{ width: '100%' }} className="close switchinfo2">
                                         <Card.Body>
                                             <Card.Title>Abdusalom Sappahonov</Card.Title>
                                             <Card.Text>
                                                 Some quick example text to build on the card title and make up the bulk of
                                                 the card's content.
                                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid quas saepe sapiente voluptate. Eos exercitationem harum laboriosam modi officia optio reiciendis soluta suscipit vitae voluptate. Blanditiis nostrum obcaecati quos?
                                             </Card.Text>
                                         </Card.Body>
                                         <Card.Footer style={{padding:'0'}}>
                                             <Button className="infoBtn"><h3>Bog'lanish</h3></Button>
                                         </Card.Footer>
                                     </Card>
                                 </Card.Text>
                             </Card.Body>
                             <Card.Body>
                                 <Card.Link href="#">
                                     <ul className="linkUl1" >
                                         <li ><div className="iconLink1"><FaTelegram/></div></li>
                                         <li>Telegram</li>
                                         <li><div className="iconLink1"><FaInstagram/></div></li>
                                         <li>Instagram</li>
                                         <li><div className="iconLink1"><FaFacebook/></div></li>
                                         <li> Facebook</li>
                                     </ul>
                                 </Card.Link></Card.Body>
                         </Card>
                     </div>
                     <div className="col-lg-6 col-12">
                         <Card style={{ width: '100%' ,border:'0px',marginTop:'20px'}}>
                             <Card.Img variant="top" src="http://195.158.24.249:2025/api/client/file/prewiev/rVgA1m" />
                             <Card.Body>
                                 <Card.Text>
                                     <Card.Title> <strong  style={{color:'#AC0B94'}}>ABDUSALOM SAPPAHONOV  <br/> <p style={{color:'black'}}> TEACHER</p></strong></Card.Title>
                                     <Form.Check
                                         onChange={this.OpenInfo3}
                                         className="formcheck"
                                         type="switch"
                                         id="custom-switch4"
                                         label="Qisqacha ma'lumot"
                                     />
                                     <Card style={{ width: '100%',marginBottom:'30px'}} className="close switchinfo3">
                                         <Card.Body>
                                             <Card.Title>Abdusalom Sappahonov</Card.Title>
                                             <Card.Text>
                                                 Some quick example text to build on the card title and make up the bulk of
                                                 the card's content.
                                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid quas saepe sapiente voluptate. Eos exercitationem harum laboriosam modi officia optio reiciendis soluta suscipit vitae voluptate. Blanditiis nostrum obcaecati quos?
                                             </Card.Text>
                                         </Card.Body>
                                         <Card.Footer style={{padding:'0'}}>
                                             <Button className="infoBtn"><h3>Bog'lanish</h3></Button>
                                         </Card.Footer>
                                     </Card>
                                 </Card.Text>
                             </Card.Body>
                             <Card.Body>
                                 <Card.Link href="#">
                                     <ul className="linkUl1" >
                                         <li ><div className="iconLink1"><FaTelegram/></div></li>
                                         <li>Telegram</li>
                                         <li><div className="iconLink1"><FaInstagram/></div></li>
                                         <li>Instagram</li>
                                         <li><div className="iconLink1"><FaFacebook/></div></li>
                                         <li> Facebook</li>
                                     </ul>
                                 </Card.Link> </Card.Body>
                         </Card>
                     </div>
                     <Pagination
                         activePage={this.state.activePage}
                         itemsCountPerPage={2}
                         totalItemsCount={20}
                         // pageRangeDisplayed={5}
                         onChange={this.handlePageChange.bind(this)}
                         defaultCurrent={1} total={20} />
                 </div>
             </div>
            </section>
        )
    }
}
export default Teachers;