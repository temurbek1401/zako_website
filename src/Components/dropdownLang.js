import React from 'react';
import Select from 'react-select';
import '../css/drop.css'

// const options = [
//   { value: 'chocolate', label: 'Chocolate' },
//   { value: 'strawberry', label: 'Strawberry' },
//   { value: 'vanilla', label: 'Vanilla' },
// ];

class DropdownLang extends React.Component {
  state = {
    selectedOption: null,
  };
  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };
  render() {
    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' },
      ];
    const { selectedOption } = this.state;

    return (
      <Select
        className='mySelect'
        value={selectedOption}
        onChange={this.handleChange}
        options={options}
      />
    );
  }
}
export default DropdownLang;