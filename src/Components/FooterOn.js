import React, { Component } from 'react'
import '../css/footerOn.css'

class FooterOn extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className='footerOnColor'>
                <div className='container'>
                    <p className='text-center contactTitle m-0 pt-5'>Agar sizda biron bir savol bo'lsa, iltimos biz bilan bog'laning</p>
                    <div className='row text-center'>
                        <div className='col-12 dad col-md-4 my-5'>
                            <input type='text' className='myInput' placeholder='Ismingizni kiriting' />
                        </div>
                        <div className='col-12 dad col-md-4 my-5'>
                            <input type='text' className='myInput' placeholder='Elektron pochtangizni kiriting' />
                        </div>
                        <div className='col-12 dad col-md-4 my-5'>
                            <input type='text' className='myInput' placeholder='Mavzuni kiriting' />
                        </div>
                    </div>
                    <div className='text-center'>
                        <button className='myBtn btn text-white biggerBtn'>So'rov yuboring</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default FooterOn;